# Cassava-PHG

## phg-build
This directory contains two scripts needed to construct a PHG in cassava.
-Create_DB_pipeline.sh  
	This script takes in all the necessary files to create a PHG db using GVCF, config file, reference genome, reference ranges, Reference load data 

-Extract_Homozygous_GVCF.sh
	This script takes in a GVCF and a reference range bed file and excracts regions of the GVCF that fall below a certain heterozygosity threshold

## phg-test
This directory contains script used to run and test the PHG
-downsample.bash
	This script downsamples a cassava BAM to simulate a certain level of single end skim coverage and produces a fastq.
-Create_keyfile.R  
	This script creates a ketfile from a directory of fastqs.  This key file is used to align and path data through the PHG.
-PHG_Map+Path.sh  
	This script creates a pangenome, aligns reads to it from keyfile, finds most likley paths through the graph, then exports a vcf of the imputed calls at all reference ranges.
-Imputation_accuracy.R  
	This script takes in a True set of SNPs with a freq file created by vcftools, as well an imputed vcf and reports the imputation accuracy at homozygous major , heterozygous, and homozygous minor sites.
	
## phg-sim
This directory contains scripts used in SLiM simulation
-Cassava_c10.slim  
	SLiM script used to simulate 5 generations of random mating from an input VCF
-genReads.sh
	This script was used to simulate skim sequence from the simulated offspring VCF.

## files
This directory contains some necessary files for these analyses
-GeneticMap_c10.txt  
	Genetic map for chromosome 10 used in SLiM simulation to represent cassava recombination rates
-HMII_taxa.txt  
	List of all 241 taxa used in the PHG db.  Consistent with all taxa used in HapMapII publication.
-ReferenceRanges_Cassava.bed 
	References ranges used in this study.  Produced using genic regions with some flanking sequence depending on flanking variability. 
-Simulation_parents.txt
	List of 20 parents used in SLiM simulation

Other large files used inclding reads,BAMs,VCF, and phenotypes are available through cassavabase.org

