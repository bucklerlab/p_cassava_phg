#1-Config_file
#2-Reference fasta
#3-RR BED file
#4-LoadGenomeData meta
#5-KeyFile
#6-GVCF Dir
TASSEL="/path/to/tassel"
###This step Creates the DB and loads the reference with reference ranges
${TASSEL}/run_pipeline.pl -Xmx10G -debug -GetDBConnectionPlugin -config $1 -create true  -endPlugin -LoadGenomeIntervalsToPHGdbPlugin -ref $2 -anchors $3 -genomeData $4  -outputDir DockerOutput -endPlugin
##This step Loads GVCFs into the DB to create Haplotypes.  Different uses of the PHG can start from different inputs, but since the
##Het cassava PHG requires GVCF filtering, we start with GVCFs
${TASSEL}/run_pipeline.pl -debug -Xmx100g -configParameters  $1 -LoadHaplotypesFromGVCFPlugin -wgsKeyFile $5 -gvcfDir $6 -referenceFasta  $2 -bedFile $3 -haplotypeMethodName HAPMAPII -haplotypeMethodDescription "Method_Description" -numThreads 20 -mergeRefBlocks true -endPlugin

##This step collapses similar haploypes
##Paramered mxDIV gives collapsing level of similarity.  see phg wiki for more details
${TASSEL}/run_pipeline.pl -Xmx100g -debug \
-HaplotypeGraphBuilderPlugin \
    -configFile $1 \
    -methods HAPMAPII \
    -includeVariantContexts true \
    -chromosomes 10 \
    -endPlugin \
-RunHapConsensusPipelinePlugin \
    -referenceFasta  $2 \
    -dbConfigFile $1 \
    -collapseMethod upgma_0.001 \
    -mxDiv .001 \
    -maxClusters 30 \
    -maxThreads 10 \
    -collapseMethodDetails "\"upgma for creating Consensus\"" \
    -endPlugin

