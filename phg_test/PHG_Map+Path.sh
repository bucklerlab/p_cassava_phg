#1- fastq directory to be mapped
#2- name of accuracy output table
#3- PHG config file
#4-Consensus Method name

TASSEL="path_to_tassel"
###Write Consensus hapltoypes to a fasta
${TASSEL}/run_pipeline.pl -Xmx100g -debug -HaplotypeGraphBuilderPlugin -configFile $3 -methods $4 -includeVariantContexts false -endPlugin -WriteFastaFromGraphPlugin -outputFile pangenome -endPlugin > PHG_pangeome.log
#Create Index
minimap2 -d pangenome_index  -k 21 -w 11 -I 90G pangenome.fa
Rscript  Create_keyfile.R $1
#Map reads to haplotypes
${TASSEL}/run_pipeline.pl -debug -Xmx200G -configParameters $3 -HaplotypeGraphBuilderPlugin -configFile $3 -methods $4 -includeVariantContexts false -includeSequences false  -endPlugin -FastqToMappingPlugin -minimap2IndexFile pangenome_index -minimapLocation /programs/minimap2-2.17/minimap2 -keyFile ./keyfile.txt -fastqDir $1 -methodName $2 -methodDescription $2 -debugDir debug/ -endplugin > PHG_DipPath.log
###Find Path through the graph
###This step can be modified to use paths from a separate PHG run as input rather than the default DB
${TASSEL}/run_pipeline.pl -debug -Xmx240g -configParameters $3 -HaplotypeGraphBuilderPlugin -configFile $3 -methods $4,refRegionGroup -includeVariantContexts false -includeSequences false -endPlugin  -DiploidPathPlugin -maxHap 31 -keyFile ./keyfile_pathKeyFile.txt -readMethod $2 -pathMethod $2 -endPlugin >> PHG_DipPath.log

###Create VCF from paths
${TASSEL}/run_pipeline.pl -Xmx10g -debug -configParameters $3 -HaplotypeGraphBuilderPlugin -configFile $3 -includeSequences false -includeVariantContexts true -methods $4 -endPlugin -ImportDiploidPathPlugin -pathMethodName $2 -endPlugin -PathsToVCFPlugin -outputFile $2 -endPlugin

