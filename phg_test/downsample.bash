###This script downsamples a BAM file and outputs a fasrq of the sampled reads

#Argument 1 is bam to downsample
#Argument 2 is the coverage level you would like to produce
echo $1
basename=`echo $1 | rev | cut -f 1 -d "/" | rev | cut -f 1 -d "_"`
FILE=$1".stat"
FILESIZE=$(stat -c%s $FILE)
#echo $FILESIZE
if [ -f "$FILE" ] && [ $FILESIZE -ne 0 ]; then
	echo "already have stat file"
else
	samtools stats $1 > $1".stat"
fi

calc(){ awk "BEGIN { print "$*" }"; }
echo $basename
totalbases=`grep "bases mapped (cigar):" $1".stat" | cut -f 3`
ref=700000000
samtools view -s `calc $ref/$totalbases*$2` -O BAM $1 > $basename"_"$2"X.bam"
samtools bam2fq $basename"_"$2"X.bam" > $basename"_"$2"X.fq"
gzip $basename"_"$2"X.fq"

