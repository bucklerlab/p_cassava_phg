####
#This Scripts takes in 2 positional arguments
#1-gvcf
#2-BED file with reference ranges
##It will take the gvcf and filter to Reference ranges with <${IBD_Threshold} heterozygous SNPs/basepair
##A 4th column of "," is added to the BED file, this can be done manually to allow for non-redundant paralellization of this step
taxa=$1
# max number of heterozygous variants per bp to threshold as IBD
IBD_Threshold=0.001
awk -v OFS="\t" '{print $1,$2,$3,","}' $2 > temp.bed 
RRBED=temp.bed
echo $RRBED
basename=`echo $1 | cut -d "." -f 1`

##Grab sites that re in the RR and calculate number of sites and number of heterozygotes in the ###Extract RefRanges that fall below a given heterozygosity threshold
bedtools intersect -a $taxa -b $RRBED | \
awk 'BEGIN{OFS="\t"} $1!~"^#" {n=split($10, G,":|/"); if(G[1]==G[2]){H=0} else{H=1}; print $1, $RRBED, $RRBED+1, H}' | \
cat - $RRBED | bedtools sort -i - | bedtools merge -c 4 -o collapse -i -  | \
awk 'BEGIN{OFS="\t"} $4~"," {n=split($4, G,",");s=0; for(i in G){s+=G[i]}; $4=s/($3-$RRBED);$5=s;$6=n; print }' | awk '{ if ($3-$RRBED > 20) { print } }'  > $taxa"_Simplify.bed"

###Extract RefRanges that fall below a given heterozygosity threshold
echo "Make IBD VCF"
awk '{ if ($4 < ${IBD_Threshold}) { print $1,$RRBED,$3,$4} }' $taxa"_Simplify.bed"   > $taxa"_homozygous_haplotypes.bed" 
vcftools --gzvcf $taxa --bed $taxa"_homozygous_haplotypes.bed" --stdout --recode --recode-INFO-all > $basename"_IBD_${IBD_Threshold}.g.vcf"
bgzip -f $basename"_IBD_${IBD_Threshold}.g.vcf"
